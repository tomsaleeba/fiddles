#!/bin/bash
set -eu
cd "$(dirname "$0")"

export GIT_SHA=abc123

# you don't have to specify the value, it's read from the env
echo "First test: read from env with no params"
docker-compose build --no-cache

# you can override the value though, note that you need to use the build-arg
# name not the env var name
echo "Second test: use param"
docker-compose build --no-cache --build-arg THE_COMMIT_ID=def456

# with no value defined, the default (from docker-compose.yml) is used
unset GIT_SHA
echo "Third test: no value defined"
docker-compose build --no-cache
