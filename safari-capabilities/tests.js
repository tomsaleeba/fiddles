;(function() {
  async function runTestSuite(platformName) {
    const someBlobBytes = [115, 111, 109, 101, 32, 98, 108, 111, 98]
    const someArrayBufferBytes = [
      115,
      111,
      109,
      101,
      32,
      97,
      114,
      114,
      97,
      121,
      66,
      117,
      102,
      102,
      101,
      114,
    ]
    const result = [
      await singleTest('string', 'some string', 'text'),
      await singleTest('string', 'some string', 'blob'),
      await singleTest('string', 'some string', 'arrayBuffer'),
      await singleTest(
        'json (stringify-ed)',
        JSON.stringify({
          foo: 'bar',
        }),
        'json',
      ),
      await singleTest('Blob (from string)', new Blob(['blah']), 'arrayBuffer'),
      await singleTest(
        'Blob (from bytes)',
        new Blob(new Uint8Array(someBlobBytes)),
        'arrayBuffer',
      ),
      await singleTest(
        'arrayBuffer',
        // TODO this doesn't decode as we expect but we don't care for this test
        new Uint8Array(someArrayBufferBytes),
        'blob',
      ),
    ]
    if (typeof FormData !== 'undefined') {
      result.push(
        await singleTest(
          'FormData (only strings)',
          function() {
            const result = new FormData()
            result.append('foo', 'bar')
            return result
          },
          'arrayBuffer',
        ),
      )
      result.push(
        await singleTest(
          'FormData (only strings)',
          function() {
            const result = new FormData()
            result.append('foo', 'bar')
            return result
          },
          'text',
        ),
      )
      result.push(
        await singleTest(
          'FormData (with File)',
          function() {
            return formDataWithFile()
          },
          'arrayBuffer',
        ),
      )
      result.push(
        await singleTest(
          'Blob (from FormData (with File))',
          async function() {
            const fd = formDataWithFile()
            // only using a Response in order to generate the blob
            const result = await new Response(fd).blob()
            // console.debug(`Blob-ified FormData=${await result.text()}`)
            return result
          },
          'arrayBuffer',
        ),
      )
      result.push(
        await singleTest(
          'FormData (with File)',
          function() {
            return formDataWithFile()
          },
          'blob',
        ),
      )
    } else {
      result.push([
        'All FormData related tests',
        'Skipped! (No FormData support)',
      ])
    }
    return result.reduce((accum, curr) => {
      accum += `\n\nTest: ${curr[0]}
        ${curr[1]}`
      return accum
    }, `\n\n\n## ${platformName} tests`)
  }

  async function singleTest(name, body, outputFnName) {
    const testName = `${name} => ${outputFnName}()`
    const prettyPrinterMapping = {
      blob: blobToText,
      text: v => v,
      json: v => JSON.stringify(v, null, 2),
      arrayBuffer: v => {
        const td = new TextDecoder()
        return td.decode(v)
      },
    }
    return [
      testName,
      await (async () => {
        try {
          const preppedBody = typeof body === 'function' ? await body() : body
          const result = await new Request('z', {
            method: 'POST',
            body: preppedBody,
          })[outputFnName]()
          const prettyPrinter = prettyPrinterMapping[outputFnName]
          if (!prettyPrinter) {
            console.warn(
              `Programmer problem, no pretty printer found for ` +
                `outputFnName=${outputFnName}`,
            )
            console.log(`${testName} result=`, result)
          } else {
            const prettyResult = await prettyPrinter(result)
            console.log(`${testName} result=${prettyResult}`)
          }
          return 'PASS'
        } catch (err) {
          return 'FAIL: ' + err.message
        }
      })(),
    ]
  }

  function formDataWithFile() {
    const fd = new FormData()
    fd.append('foo', 'bar')
    fd.append(
      'file',
      new File([new Uint8Array([1, 2, 3, 4])], 'some-file', {
        type: 'image/png',
      }),
    )
    return fd
  }

  function blobToText(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.addEventListener('loadend', e => {
        const text = e.srcElement.result
        return resolve(text)
      })
      reader.addEventListener('error', err => {
        const msg = 'Failed to convert blob to text'
        console.error(msg, err)
        return reject(msg)
      })
      reader.readAsText(blob)
    })
  }

  const global =
    typeof window === 'object' ? window : typeof self === 'object' ? self : this
  global.runTestSuite = runTestSuite
})()
