View live demos at https://tomsaleeba.gitlab.io/fiddles/

## To make an update
1. create a new directory
1. add an `index.html` file to that directory, and any other files you need
1. commit and push, GitLab CI will take care of the rest
