importScripts('https://unpkg.com/formdata-polyfill@3.0.19/FormData.js')

self.addEventListener('install', function(event) {
  self.skipWaiting()
})

self.addEventListener('activate', event => {
  clients.claim()
})

self.addEventListener('fetch', event => {
  if (!event.request.url.endsWith('//local.service-worker/run-test-in-sw')) {
    return // browser can handle
  }
  return event.respondWith(
    (async function() {
      try {
        const buffer = await new Request('https://localhost', {
          method: 'POST',
          body: (() => {
            const result = new FormData()
            result.append(
              'file',
              new File([new Uint8Array([1, 2])], 'aa', {
                type: 'image/png',
              }),
            )
            return result
          })(),
        }).arrayBuffer()
        console.log(buffer)
        return new Response(`SUCCESS: ${buffer}`, {
          headers: { 'Content-Type': 'text/html' },
        })
      } catch (err) {
        console.error(err)
        return new Response(`ERROR: ${err.message}\n${err.stack}`, {
          headers: { 'Content-Type': 'text/html' },
        })
      }
    })(),
  )
})
