// Demo showing how to encrypt a payload arbitary length using a hybrid
// approach. A symmetric key isn't good because if you share it some one
// party can encrypt data, then they can also decrypt it. The tradeoff is that
// a symmetric key can encrypt large amounts of data. Asymmetric keys are
// the opposite: they use different keys for encrypting and decrypting but they
// can only encrypt a small amount of data. So we combine the two approaches to
// encrypt our payload with the symmetric key, which we can generate on the
// spot, then we encrypt the symmetric key using our public part of the
// asymmetric key and send the encrypted symmetric key along with our
// encrypted payload.

// Hacked together from MDN examples at:
// https://github.com/mdn/dom-examples/blob/master/web-crypto/encrypt-decrypt/
;(async function() {
  console.log('Generating asymmetric key')
  const asymmetricKey = await generateAsymKey()
  // note: the publicKey is not sensitive, share it anywhere you like, but the
  // privateKey should never be shared.

  const stage1Result = await (async function encryptStage() {
    const plainText =
      'In cryptography, encryption is the process of encoding information. ' +
      'This process converts the original representation of the information, ' +
      'known as plaintext, into an alternative form known as ciphertext. ' +
      'Only authorized parties can decipher a ciphertext back to plaintext ' +
      'and access the original information. Encryption does not itself ' +
      'prevent interference but denies the intelligible content to a ' +
      'would-be interceptor.'

    console.log('Generating symmetric key')
    const symmetricKey = await generateSymKey()
    console.log('Encrypting payload with symmetric key')
    const { payloadCipherText, iv } = await encryptPayload(
      plainText,
      symmetricKey,
    )
    console.log('Encrypting symmetric key bundle')
    const symKeyBundleCipherText = await encryptSymKeyBundle(
      symmetricKey,
      iv,
      asymmetricKey.publicKey,
    )
    const safeToTransport = {
      payload: payloadCipherText,
      symKeyBundle: symKeyBundleCipherText,
    }
    console.log('Data ready to transport', safeToTransport)
    return {
      privateKey: asymmetricKey.privateKey,
      safeToTransport,
    }
  })()

  await (async function decryptStage() {
    console.log('Decrypting symmetric key bundle')
    const decryptedSymmetricKeyBundle = await decryptSymKeyBundle(
      stage1Result.safeToTransport.symKeyBundle,
      stage1Result.privateKey,
    )
    console.log('Decrypting payload')
    const decryptedPayload = await decryptPayload(
      stage1Result.safeToTransport.payload,
      decryptedSymmetricKeyBundle,
    )
    console.log('Decrypted payload', decryptedPayload)
  })()

  function generateSymKey() {
    return window.crypto.subtle.generateKey(
      {
        name: 'AES-GCM',
        length: 256,
      },
      true,
      ['encrypt', 'decrypt'],
    )
  }

  async function encryptPayload(plainText, symKey) {
    const encodedMsg = new TextEncoder().encode(plainText)
    const iv = window.crypto.getRandomValues(new Uint8Array(12))
    const encrypted = await window.crypto.subtle.encrypt(
      {
        name: 'AES-GCM',
        iv,
      },
      symKey,
      encodedMsg,
    )
    return {
      payloadCipherText: arrayBufferToBase64(encrypted),
      iv,
    }
  }

  function generateAsymKey() {
    return window.crypto.subtle.generateKey(
      {
        name: 'RSA-OAEP',
        hash: 'SHA-256',
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
      },
      true,
      ['encrypt', 'decrypt'],
    )
  }

  async function encryptSymKeyBundle(symKey, iv, pubKey) {
    const exportedSymKey = await window.crypto.subtle.exportKey('raw', symKey)
    const base64ExportedSymKey = arrayBufferToBase64(exportedSymKey)
    const base64Iv = arrayBufferToBase64(iv)
    const encodedMsg = new TextEncoder().encode(
      JSON.stringify({ symKey: base64ExportedSymKey, iv: base64Iv }),
    )
    console.log('Size of sym key bundle message', encodedMsg.byteLength)
    if (encodedMsg.byteLength > 190) {
      console.warn(
        'sym key bundle is too large, the encrypt is probably about to fail',
      )
    }
    const encrypted = await window.crypto.subtle.encrypt(
      // I chose this over wrapKey because I can include the IV
      {
        name: 'RSA-OAEP',
      },
      pubKey,
      encodedMsg,
    )
    return arrayBufferToBase64(encrypted)
  }

  async function decryptSymKeyBundle(symKeyBundleCipherTextBase64, privateKey) {
    // we base64 encoded the symKeyBundleCipherText for transport
    const symKeyBundleCipherText = base64StrToArrayBuffer(
      symKeyBundleCipherTextBase64,
    )
    const decryptedBundleStr = await window.crypto.subtle.decrypt(
      {
        name: 'RSA-OAEP',
      },
      privateKey,
      symKeyBundleCipherText,
    )
    const bundle = JSON.parse(new TextDecoder().decode(decryptedBundleStr))
    const importedKey = await window.crypto.subtle.importKey(
      'raw',
      base64StrToArrayBuffer(bundle.symKey),
      {
        name: 'AES-GCM',
        length: 256,
      },
      true,
      ['encrypt', 'decrypt'],
    )
    return {
      symKey: importedKey,
      iv: base64StrToArrayBuffer(bundle.iv),
    }
  }

  async function decryptPayload(payload, symKeyBundle) {
    // we base64 encoded the payload for transport
    const parsedEncrypted = base64StrToArrayBuffer(payload)
    const decrypted = await window.crypto.subtle.decrypt(
      {
        name: 'AES-GCM',
        iv: symKeyBundle.iv,
      },
      symKeyBundle.symKey,
      parsedEncrypted,
    )
    const decryptedPlainText = new TextDecoder().decode(decrypted)
    return decryptedPlainText
  }

  function base64StrToArrayBuffer(base64Str) {
    // thanks for the conversion https://stackoverflow.com/a/16245768/1410035
    const byteCharacters = atob(base64Str)
    const byteNumbers = new Array(byteCharacters.length)
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i)
    }
    const byteArray = new Uint8Array(byteNumbers)
    return byteArray
  }

  function arrayBufferToBase64(buffer) {
    // thanks https://stackoverflow.com/a/9458996/1410035
    let binary = ''
    const bytes = new Uint8Array(buffer)
    const len = bytes.byteLength
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i])
    }
    return window.btoa(binary)
  }
})()
