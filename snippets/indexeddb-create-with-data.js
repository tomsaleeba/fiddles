;(function() {
  // This is what our customer data looks like.
  const customerData = [
    { ssn: '444-44-4444', name: 'Bill', age: 35, email: 'bill@company.com' },
    { ssn: '555-55-5555', name: 'Donna', age: 32, email: 'donna@home.org' },
  ]

  const dbName = 'the_name'
  console.log('opening DB')
  var request = indexedDB.open(dbName, 2)

  request.onerror = function(event) {
    // FIXME how is the error sent to us?
    console.error('Failed to open DB', event)
  }

  request.onupgradeneeded = function(event) {
    console.log('upgrading DB schema')
    const db = event.target.result

    // Create an objectStore to hold information about our customers. We're
    // going to use "ssn" as our key path because it's guaranteed to be
    // unique - or at least that's what I was told during the kickoff meeting.
    var objectStore = db.createObjectStore('customers', { keyPath: 'ssn' })

    // Create an index to search customers by name. We may have duplicates
    // so we can't use a unique index.
    objectStore.createIndex('name', 'name', { unique: false })

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex('email', 'email', { unique: true })
  }

  request.onsuccess = function() {
    console.log('adding data')
    const db = event.target.result

    // Store values in the newly created objectStore.
    var customerObjectStore = db
      .transaction('customers', 'readwrite')
      .objectStore('customers')
    customerData.forEach(function(customer) {
      console.log('adding customer ', customer)
      customerObjectStore.add(customer)
    })

    db.close()
  }
})()
