// demo showing we can generate a crypto key, export and import it and use the imported key
// hacked together from MDN examples: https://github.com/mdn/dom-examples/blob/master/web-crypto/encrypt-decrypt/rsa-oaep.js
;(async function() {
  const algorithm = 'RSA-OAEP'
  const rsaParams = {
    name: algorithm,
    hash: 'SHA-256',
  }
  console.log('Generating key pair')
  const keyPair = await window.crypto.subtle.generateKey(
    {
      ...rsaParams,
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
    },
    true,
    ['encrypt', 'decrypt'],
  )
  console.log('keypair is', keyPair)

  const publicJwk = await window.crypto.subtle.exportKey(
    'jwk',
    keyPair.publicKey,
  )
  console.log('public JWK is', JSON.stringify(publicJwk, null, 2))

  const importedKey = await window.crypto.subtle.importKey(
    'jwk',
    publicJwk,
    rsaParams,
    true,
    ['encrypt'],
  )

  const plainText = 'hello, world!'
  const encodedMsg = new TextEncoder().encode(plainText)
  const encrypted = await window.crypto.subtle.encrypt(
    {
      name: algorithm,
    },
    importedKey,
    encodedMsg,
  )
  // encrypted is an ArrayBuffer, let's base64 encode to be more portable
  const encryptedBase64 = arrayBufferToBase64(encrypted)
  console.log('encrypted message is', encryptedBase64)

  // we need to convert the base64 ciphertext into an ArrayBuffer
  const parsedEncrypted = base64StrToArrayBuffer(encryptedBase64)
  const decrypted = await window.crypto.subtle.decrypt(
    {
      name: algorithm,
    },
    keyPair.privateKey,
    parsedEncrypted,
  )
  console.log('decrypted is', decrypted)

  const decryptedPlainText = new TextDecoder().decode(decrypted)
  console.log('decrypted plain text is', decryptedPlainText)

  function base64StrToArrayBuffer(base64Str) {
    // thanks for the conversion https://stackoverflow.com/a/16245768/1410035
    const byteCharacters = atob(base64Str)
    const byteNumbers = new Array(byteCharacters.length)
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i)
    }
    const byteArray = new Uint8Array(byteNumbers)
    return byteArray
  }

  function arrayBufferToBase64(buffer) {
    // thanks https://stackoverflow.com/a/9458996/1410035
    let binary = ''
    const bytes = new Uint8Array(buffer)
    const len = bytes.byteLength
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i])
    }
    return window.btoa(binary)
  }
})()
