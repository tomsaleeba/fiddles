#!/bin/bash
set -euo pipefail
cd `dirname "$0"`

tmpDir=.public
outDir=public

echo "[INFO] copying static files"
rm -fr $tmpDir $outDir
mkdir $tmpDir
find . \
  -maxdepth 1 \
  -type d \
  -not -name '.*' \
  -not -name public | xargs -I '{}' cp -r {} $tmpDir/

echo "[INFO] collecting links"
links=$(cd $tmpDir && find .\
  -type f \
  -name index.html \
  -exec echo "<li><a href=\"{}\">{}</a></ali>" \;)

echo "[INFO] writing index page"
cat << EOF > $tmpDir/index.html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Techotom Fiddles</title>
</head>

<body>
  <p>Fiddles for testing stuff.</p>
  <ul>
    $links
  </ul>
</body>

</html>
EOF

echo "[INFO] promoting temp dir to final output"
mv $tmpDir $outDir

echo "[INFO] done :D"
